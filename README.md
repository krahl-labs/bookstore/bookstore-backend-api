# Bookstore Backend API

## Requirements

- NodeJS 20
- MySQL 5.7


## Start project

```
npm install
npm run start:dev
```