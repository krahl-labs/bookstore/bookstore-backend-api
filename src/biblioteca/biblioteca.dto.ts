import { UsuarioEntity } from "src/usuario/usuario.entity";

export interface BibliotecaDTO {
    idBiblioteca?: number,
    nome: string,
    usuario: UsuarioEntity
}