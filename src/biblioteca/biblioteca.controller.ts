import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { BibliotecaService } from "./biblioteca.service";
import { BibliotecaDTO } from "./biblioteca.dto";
import { BibliotecaEntity } from "./biblioteca.entity";

@Controller('biblioteca')
export class BibliotecaController {
  constructor(private readonly service: BibliotecaService) {}

  @Post()
  create(@Body() form: BibliotecaDTO): Promise<BibliotecaEntity> {
    return this.service.createAndupdate(null ,form);
  }

  @Put(':id')
  update(@Param('id') id: number, @Body() form: BibliotecaDTO): Promise<BibliotecaEntity> {
    return this.service.createAndupdate(id, form);
  }

  @Get()
  findAll(): Promise<BibliotecaEntity[]> {
    return this.service.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<BibliotecaEntity> {
    return this.service.findOne(id);
  } 

  @Delete(':id')
  remove(@Param('id') id: number): Promise<void> {
    return this.service.remove(id);
  }
}