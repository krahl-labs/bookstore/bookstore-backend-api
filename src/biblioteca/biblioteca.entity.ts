import { LivroLeituraEntity } from "src/livro_leitura/livro.leitura.entity";
import { UsuarioEntity } from "src/usuario/usuario.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "biblioteca"})
export class BibliotecaEntity {
    @PrimaryGeneratedColumn({ type: 'integer', name: 'id_biblioteca' })
    idBiblioteca: number;

    @Column({ name: 'ds_nome' })
    nome: string;

    @ManyToOne(type => UsuarioEntity, usuario => usuario.bibliotecas)
    @JoinColumn({ name: 'id_usuario' })
    usuario: UsuarioEntity;

    @ManyToOne(type => LivroLeituraEntity, leitura => leitura.biblioteca)
    @JoinColumn({ name: 'id_livro_leitura' })
    leituras: LivroLeituraEntity[];
}