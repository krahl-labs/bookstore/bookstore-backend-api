import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { BibliotecaController } from "./biblioteca.controller";
import { BibliotecaService } from "./biblioteca.service";
import { BibliotecaEntity } from "./biblioteca.entity";
import { UsuarioEntity } from "src/usuario/usuario.entity";

@Module({
    imports: [TypeOrmModule.forFeature([BibliotecaEntity, UsuarioEntity])],
    providers: [BibliotecaService],
    controllers: [BibliotecaController],
  })
  export class BibliotecaModule {}
  