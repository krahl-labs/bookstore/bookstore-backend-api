import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BibliotecaEntity } from './biblioteca.entity';
import { BibliotecaDTO } from './biblioteca.dto';
@Injectable()
export class BibliotecaService {
    constructor(
        @InjectRepository(BibliotecaEntity)
        private readonly repository: Repository<BibliotecaEntity>,
    ) { }

    async createAndupdate(id: number, form: BibliotecaDTO): Promise<BibliotecaEntity> {
        if (id) {//update
            const registro = await this.findOne(id);
            return this.repository.save({
                ...registro,
                ...form
            });
        } else {//create     
            return this.repository.save({
                ...form
            });
        }
    }

    async findAll(): Promise<BibliotecaEntity[]> {
        return this.repository.find();
    }

    findOne(id: number): Promise<BibliotecaEntity> {
        return this.repository.findOne({where: {idBiblioteca: id}});
    }

    async remove(id: number): Promise<void> {
        await this.repository.delete(id);
    }
}