import { AutorEntity } from "src/autor/autor.entity";
import { LivroLeituraEntity } from "src/livro_leitura/livro.leitura.entity";
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "livro"})
export class LivroEntity {
    @PrimaryGeneratedColumn({ type: 'integer', name: 'id_livro' })
    idLivro: number;

    @Column({ name: 'ds_titulo' })
    titulo: string;

    @Column({ name: 'ds_subtitulo' })
    subtitulo: string;

    @Column({ name: 'ds_isbn10', nullable: true })
    isbn10: string;

    @Column({ name: 'ds_isbn13', nullable: true })
    isbn13: string;

    @Column({ name: 'qtd_paginas' })
    qtdPaginas: number;

    @Column({ name: 'ds_thumbnail', nullable: true })
    thumbnail: string;

    @Column({ name: 'ds_googleid', nullable: true })
    googleId: string;

    @ManyToMany(type => AutorEntity, autor => autor.livros)
    @JoinTable({name: "author_livros"})
    autores: AutorEntity[];

    @ManyToOne(type => LivroLeituraEntity, leitura => leitura.livro)
    @JoinColumn({ name: 'id_livro_leitura' })
    leituras: LivroLeituraEntity[];
}