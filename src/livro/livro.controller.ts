import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { LivroService } from "./livro.service";
import { LivroEntity } from "./livro.entity";
import { LivroDTO } from "./livro.dto";

@Controller('livro')
export class LivroController {
  constructor(private readonly service: LivroService) {}

  @Post()
  create(@Body() formLivro: LivroDTO): Promise<LivroEntity> {
    return this.service.createAndupdate(null ,formLivro);
  }

  @Put(':id')
  update(@Param('id') id: number, @Body() formLivro: LivroDTO): Promise<LivroEntity> {
    return this.service.createAndupdate(id, formLivro);
  }

  @Get()
  findAll(): Promise<LivroEntity[]> {
    return this.service.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<LivroEntity> {
    return this.service.findOne(id);
  } 

  @Delete(':id')
  remove(@Param('id') id: number): Promise<void> {
    return this.service.remove(id);
  }
}