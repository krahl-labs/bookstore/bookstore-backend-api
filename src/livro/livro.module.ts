import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { LivroEntity } from "./livro.entity";
import { LivroService } from "./livro.service";
import { LivroController } from "./livro.controller";
import { AutorEntity } from "src/autor/autor.entity";

@Module({
    imports: [TypeOrmModule.forFeature([AutorEntity, LivroEntity])],
    providers: [LivroService],
    controllers: [LivroController],
  })
  export class LivroModule {}
  