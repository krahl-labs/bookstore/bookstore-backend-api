import { AutorEntity } from "src/autor/autor.entity";

export interface LivroDTO {
    idLivro?: number,
    titulo: string,
    subtitulo: string,
    isbn10?: string,
    isbn13?: string,
    qtdPaginas: number,
    thumbnail?: string,
    googleId?: string,
    autores?: AutorEntity[],
}