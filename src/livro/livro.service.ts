import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LivroEntity } from './livro.entity';
import { LivroDTO } from './livro.dto';

@Injectable()
export class LivroService {
    constructor(
        @InjectRepository(LivroEntity)
        private readonly repository: Repository<LivroEntity>,
    ) { }

    async createAndupdate(id: number, form: LivroDTO): Promise<LivroEntity> {
        if (id) {//update
            const registro = await this.findOne(id);
            return this.repository.save({
                ...registro,
                ...form
            });
        } else {//create     
            return this.repository.save({
                ...form
            });
        }
    }

    async findAll(): Promise<LivroEntity[]> {
        return this.repository.find();
    }

    findOne(id: number): Promise<LivroEntity> {
        return this.repository.findOne({where: {idLivro: id}});
    }

    async remove(id: number): Promise<void> {
        await this.repository.delete(id);
    }
}