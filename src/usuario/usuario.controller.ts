import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { UsuarioEntity } from "./usuario.entity";
import { UsuarioService } from "./usuario.service";
import { UsuarioDTO } from "./usuario.dto";

@Controller('usuario')
export class UsuarioController {
  constructor(private readonly service: UsuarioService) {}

  @Post()
  create(@Body() formUsuario: UsuarioDTO): Promise<UsuarioEntity> {
    return this.service.createAndupdate(null ,formUsuario);
  }

  @Put(':id')
  update(@Param('id') id: number, @Body() formUsuario: UsuarioDTO): Promise<UsuarioEntity> {
    return this.service.createAndupdate(id, formUsuario);
  }

  @Get()
  findAll(): Promise<UsuarioEntity[]> {
    return this.service.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<UsuarioEntity> {
    return this.service.findOne(id);
  } 

  @Delete(':id')
  remove(@Param('id') id: number): Promise<void> {
    return this.service.remove(id);
  }
}