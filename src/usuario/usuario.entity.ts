import { BibliotecaEntity } from "src/biblioteca/biblioteca.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "usuario"})
export class UsuarioEntity {
    @PrimaryGeneratedColumn({ type: 'integer', name: 'id_usuario' })
    idUsuario: number;

    @Column({ name: 'ds_nome' })
    nome: string;

    @Column({ name: 'ds_login' })
    login: string;

    @Column({ name: 'ds_senha' })
    senha: string;

    @Column({ type: 'boolean', name: 'fg_ativo', default: true })
    ativo: boolean;

    @OneToMany(type => BibliotecaEntity, biblioteca => biblioteca.usuario)
    bibliotecas: BibliotecaEntity[]
}