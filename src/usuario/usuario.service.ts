import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsuarioEntity } from './usuario.entity';
import { UsuarioDTO } from './usuario.dto';

@Injectable()
export class UsuarioService {
    constructor(
        @InjectRepository(UsuarioEntity)
        private readonly repository: Repository<UsuarioEntity>,
    ) { }

    async createAndupdate(id: number, form: UsuarioDTO): Promise<UsuarioEntity> {
        if (id) {//update
            const user = await this.findOne(id);
            return this.repository.save({
                ...user,
                ...form
            });
        } else {//create     
            return this.repository.save({
                ...form
            });
        }
    }

    async findAll(): Promise<UsuarioEntity[]> {
        return this.repository.find();
    }

    findOne(id: number): Promise<UsuarioEntity> {
        return this.repository.findOne({where: {idUsuario: id}});
    }

    async remove(id: number): Promise<void> {
        await this.repository.delete(id);
    }
}