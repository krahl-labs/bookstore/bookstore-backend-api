export interface UsuarioDTO {
    idUsuario?: number,
    nome: string,
    login: string,
    senha: string,
    ativo?: boolean,
}