import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AnotacaoEntity } from "./anotacao.entity";
import { LivroLeituraEntity } from "src/livro_leitura/livro.leitura.entity";
import { AnotacaoService } from "./anotacao.service";
import { AnotacaoController } from "./anotacao.controller";

@Module({
    imports: [TypeOrmModule.forFeature([AnotacaoEntity, LivroLeituraEntity])],
    providers: [AnotacaoService],
    controllers: [AnotacaoController],
  })
  export class AnotacaoModule {}
  