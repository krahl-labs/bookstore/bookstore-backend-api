import { LivroLeituraEntity } from "src/livro_leitura/livro.leitura.entity";

export interface AnotacaoDTO {
    idAnotacao?: number,
    nome: string,
    leitura: LivroLeituraEntity,
}