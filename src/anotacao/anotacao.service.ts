import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AnotacaoEntity } from './anotacao.entity';
import { AnotacaoDTO } from './anotacao.dto';

@Injectable()
export class AnotacaoService {
    constructor(
        @InjectRepository(AnotacaoEntity)
        private readonly repository: Repository<AnotacaoEntity>,
    ) { }

    async createAndupdate(id: number, form: AnotacaoDTO): Promise<AnotacaoEntity> {
        if (id) {//update
            const registro = await this.findOne(id);
            return this.repository.save({
                ...registro,
                ...form
            });
        } else {//create     
            return this.repository.save({
                ...form
            });
        }
    }

    async findAll(): Promise<AnotacaoEntity[]> {
        return this.repository.find();
    }

    findOne(id: number): Promise<AnotacaoEntity> {
        return this.repository.findOne({where: {idAnotacao: id}});
    }

    async remove(id: number): Promise<void> {
        await this.repository.delete(id);
    }
}