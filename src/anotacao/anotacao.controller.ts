import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { AnotacaoService } from "./anotacao.service";
import { AnotacaoDTO } from "./anotacao.dto";
import { AnotacaoEntity } from "./anotacao.entity";

@Controller('anotacao')
export class AnotacaoController {
  constructor(private readonly service: AnotacaoService) {}

  @Post()
  create(@Body() form: AnotacaoDTO): Promise<AnotacaoEntity> {
    return this.service.createAndupdate(null ,form);
  }

  @Put(':id')
  update(@Param('id') id: number, @Body() form: AnotacaoDTO): Promise<AnotacaoEntity> {
    return this.service.createAndupdate(id, form);
  }

  @Get()
  findAll(): Promise<AnotacaoEntity[]> {
    return this.service.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<AnotacaoEntity> {
    return this.service.findOne(id);
  } 

  @Delete(':id')
  remove(@Param('id') id: number): Promise<void> {
    return this.service.remove(id);
  }
}