import { LivroLeituraEntity } from "src/livro_leitura/livro.leitura.entity";
import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "anotacao"})
export class AnotacaoEntity {
    @PrimaryGeneratedColumn({ type: 'integer', name: 'id_anotacao' })
    idAnotacao: number;

    @Column({ name: 'ds_descricao' })
    descricao: string;

    @Column({ name: 'vl_pagina' })
    pagina: number;

    @OneToMany(type => LivroLeituraEntity, leitura => leitura.anotacoes)
    @JoinColumn({name: "id_livro_leitura"})
    leitura: LivroLeituraEntity;
}