import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioEntity } from './usuario/usuario.entity';
import { UsuarioModule } from './usuario/usuario.module';
import { AutorEntity } from './autor/autor.entity';
import { AutorModule } from './autor/autor.module';
import { LivroModule } from './livro/livro.module';
import { LivroEntity } from './livro/livro.entity';
import { BibliotecaModule } from './biblioteca/biblioteca.module';
import { BibliotecaEntity } from './biblioteca/biblioteca.entity';
import { LivroLeituraEntity } from './livro_leitura/livro.leitura.entity';
import { LivroLeituraModule } from './livro_leitura/livro.leitura.module';
import { AnotacaoEntity } from './anotacao/anotacao.entity';
import { AnotacaoModule } from './anotacao/anotacao.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [
        UsuarioEntity,
        AutorEntity,
        LivroEntity,
        BibliotecaEntity,
        LivroLeituraEntity,
        AnotacaoEntity,
      ],
      synchronize: true,
    }),
    UsuarioModule,
    AutorModule,
    LivroModule,
    BibliotecaModule,
    LivroLeituraModule,
    AnotacaoModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
