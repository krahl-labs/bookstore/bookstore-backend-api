import { AnotacaoEntity } from "src/anotacao/anotacao.entity";
import { BibliotecaEntity } from "src/biblioteca/biblioteca.entity";
import { LivroEntity } from "src/livro/livro.entity";
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "livro_leitura"})
export class LivroLeituraEntity {
    @PrimaryGeneratedColumn({ type: 'integer', name: 'id_livro_leitura' })
    idLivroLeitura: number;

    @Column({ name: 'dt_inicio', type: 'timestamp', default: () => "CURRENT_TIMESTAMP" })
    dataInicio: string;

    @Column({ name: 'dt_fim', type: 'timestamp', nullable: true })
    dataFim: string;

    @Column({ name: 'vl_pagina_atual', nullable: true })
    paginaAtual: number;

    @OneToMany(type => LivroEntity, livro => livro.leituras)
    @JoinColumn({name: "id_livro"})
    livro: LivroEntity;

    @OneToMany(type => BibliotecaEntity, biblioteca => biblioteca.leituras)
    @JoinColumn({name: "id_biblioteca"})
    biblioteca: BibliotecaEntity;

    @ManyToOne(type => AnotacaoEntity, anotacao => anotacao.leitura)
    @JoinColumn({ name: 'id_anotacao' })
    anotacoes: AnotacaoEntity[];
}