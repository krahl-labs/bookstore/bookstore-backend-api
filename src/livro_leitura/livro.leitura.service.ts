import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LivroLeituraEntity } from './livro.leitura.entity';
import { LivroLeituraDTO } from './livro.leitura.dto';

@Injectable()
export class LivroLeituraService {
    constructor(
        @InjectRepository(LivroLeituraEntity)
        private readonly repository: Repository<LivroLeituraEntity>,
    ) { }

    async createAndupdate(id: number, form: LivroLeituraDTO): Promise<LivroLeituraEntity> {
        if (id) {//update
            const registro = await this.findOne(id);
            return this.repository.save({
                ...registro,
                ...form
            });
        } else {//create     
            return this.repository.save({
                ...form
            });
        }
    }

    async findAll(): Promise<LivroLeituraEntity[]> {
        return this.repository.find();
    }

    findOne(id: number): Promise<LivroLeituraEntity> {
        return this.repository.findOne({where: {idLivroLeitura: id}});
    }

    async remove(id: number): Promise<void> {
        await this.repository.delete(id);
    }
}