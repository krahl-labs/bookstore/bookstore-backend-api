import { BibliotecaEntity } from "src/biblioteca/biblioteca.entity";
import { LivroEntity } from "src/livro/livro.entity";

export interface LivroLeituraDTO {
    idLivroLeitura?: number,
    dataInicio?: string,
    dataFim?: string,
    paginaAtual: number,
    livro?: LivroEntity,
    biblioteca?: BibliotecaEntity,
}