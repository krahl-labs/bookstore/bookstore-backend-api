import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { LivroLeituraService } from "./livro.leitura.service";
import { LivroLeituraDTO } from "./livro.leitura.dto";

@Controller('livro-leitura')
export class LivroLeituraController {
  constructor(private readonly service: LivroLeituraService) {}

  @Post()
  create(@Body() form: LivroLeituraDTO): Promise<LivroLeituraDTO> {
    return this.service.createAndupdate(null ,form);
  }

  @Put(':id')
  update(@Param('id') id: number, @Body() form: LivroLeituraDTO): Promise<LivroLeituraDTO> {
    return this.service.createAndupdate(id, form);
  }

  @Get()
  findAll(): Promise<LivroLeituraDTO[]> {
    return this.service.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<LivroLeituraDTO> {
    return this.service.findOne(id);
  } 

  @Delete(':id')
  remove(@Param('id') id: number): Promise<void> {
    return this.service.remove(id);
  }
}