import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { LivroEntity } from "src/livro/livro.entity";
import { LivroLeituraService } from "./livro.leitura.service";
import { LivroLeituraController } from "./livro.leitura.controller";
import { LivroLeituraEntity } from "./livro.leitura.entity";
import { BibliotecaEntity } from "src/biblioteca/biblioteca.entity";

@Module({
    imports: [TypeOrmModule.forFeature([LivroLeituraEntity, LivroEntity, BibliotecaEntity])],
    providers: [LivroLeituraService],
    controllers: [LivroLeituraController],
  })
  export class LivroLeituraModule {}
  