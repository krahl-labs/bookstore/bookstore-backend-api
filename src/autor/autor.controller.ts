import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { AutorService } from "./autor.service";
import { AutorEntity } from "./autor.entity";
import { AutorDTO } from "./autor.dto";

@Controller('autor')
export class AutorController {
  constructor(private readonly service: AutorService) {}

  @Post()
  create(@Body() formAutor: AutorDTO): Promise<AutorEntity> {
    return this.service.createAndupdate(null ,formAutor);
  }

  @Put(':id')
  update(@Param('id') id: number, @Body() formAutor: AutorDTO): Promise<AutorEntity> {
    return this.service.createAndupdate(id, formAutor);
  }

  @Get()
  findAll(): Promise<AutorEntity[]> {
    return this.service.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<AutorEntity> {
    return this.service.findOne(id);
  } 

  @Delete(':id')
  remove(@Param('id') id: number): Promise<void> {
    return this.service.remove(id);
  }
}