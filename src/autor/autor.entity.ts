import { LivroEntity } from "src/livro/livro.entity";
import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "autor"})
export class AutorEntity {
    @PrimaryGeneratedColumn({ type: 'integer', name: 'id_autor' })
    idAutor: number;

    @Column({ name: 'ds_nome' })
    nome: string;

    @ManyToMany(type => LivroEntity, livro => livro.autores)
    @JoinTable({name: "author_livros"})
    livros: LivroEntity[];
}