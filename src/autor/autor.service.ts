import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AutorEntity } from './autor.entity';
import { AutorDTO } from './autor.dto';

@Injectable()
export class AutorService {
    constructor(
        @InjectRepository(AutorEntity)
        private readonly repository: Repository<AutorEntity>,
    ) { }

    async createAndupdate(id: number, form: AutorDTO): Promise<AutorEntity> {
        if (id) {//update
            const registro = await this.findOne(id);
            return this.repository.save({
                ...registro,
                ...form
            });
        } else {//create     
            return this.repository.save({
                ...form
            });
        }
    }

    async findAll(): Promise<AutorEntity[]> {
        return this.repository.find();
    }

    findOne(id: number): Promise<AutorEntity> {
        return this.repository.findOne({where: {idAutor: id}});
    }

    async remove(id: number): Promise<void> {
        await this.repository.delete(id);
    }
}