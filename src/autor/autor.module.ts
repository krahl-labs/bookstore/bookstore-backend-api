import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AutorEntity } from "./autor.entity";
import { AutorService } from "./autor.service";
import { AutorController } from "./autor.controller";
import { LivroEntity } from "src/livro/livro.entity";

@Module({
    imports: [TypeOrmModule.forFeature([AutorEntity, LivroEntity])],
    providers: [AutorService],
    controllers: [AutorController],
  })
  export class AutorModule {}
  